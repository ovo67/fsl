import numpy as np
import pandas as pd
import random
import torch
from torchvision.transforms import transforms
from torch.utils.data import Dataset
from torch.utils.data import DataLoader

from collections import Counter

import torch.nn.functional as F
from copy import deepcopy

n_way=5
n_shot=5
n_query=15
class MiniImagenet(Dataset):
    def __init__(self, mode, batchsz, n_way, k_shot, k_query, resize, startidx=0):
        self.batchsz = batchsz  # batch of set, not batch of imgs
        self.n_way = n_way  # n-way
        self.k_shot = k_shot  # k-shot
        self.k_query = k_query  # for evaluation
        self.setsz = self.n_way * self.k_shot  # num of samples per set
        self.querysz = self.n_way * self.k_query  # number of samples per set for evaluation
        self.resize = resize 
       

        if mode == 'train':
            self.transform = transforms.Compose([
                                                 transforms.ToTensor(),
                                                 transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225))
                                                 ])
        else:
            self.transform = transforms.Compose([
                                                 transforms.ToTensor(),
                                                 transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225))
                                                 ])

        self.data=pd.read_pickle('mini-imagenet-cache-'+ mode +'.pkl')#dict{'image_data":[n*84*84*3],'class_dict':{'n12345678':[0,...,599],..}}
        self.image_data=self.data['image_data']
        self.class_dict=self.data['class_dict']
        self.cls_num=len(self.class_dict)
        self.create_batch(self.batchsz)
        #self.data = []
    
    def create_batch(self, batchsz):
        """
        create batch for meta-learning.
        ×episode× here means batch, and it means how many sets we want to retain.
        :param episodes: batch size
        :return:
        """

        self.support_x_batch = []  # support set batch
        self.query_x_batch = []  # query set batch
        for b in range(batchsz):  # for each batch
            # 1.select n_way classes randomly
            selected_cls = np.random.choice(self.cls_num, self.n_way, False)  # no duplicate
            np.random.shuffle(selected_cls)
            support_x = []
            query_x = []
            for cls in selected_cls:
                # 2. select k_shot + k_query for each class
                selected_imgs_idx = np.random.choice(600, self.k_shot + self.k_query, False)
                np.random.shuffle(selected_imgs_idx)
                indexDtrain = np.array(selected_imgs_idx[:self.k_shot])  # idx for Dtrain
                indexDtest = np.array(selected_imgs_idx[self.k_shot:])  # idx for Dtest
                support_x.append(
                    cls*600+indexDtrain.tolist())  # get all images index for current Dtrain
                query_x.append(cls*600+indexDtest.tolist())

            # shuffle the correponding relation between support set and query set
            random.shuffle(support_x)
            random.shuffle(query_x)

            self.support_x_batch.append(support_x)  # append set to current sets
            self.query_x_batch.append(query_x)  # append sets to current sets
            #support_x_batch  shape=(batchsz,way,shot)
        
    def __getitem__(self, index):
        """
        index means index of sets, 0<= index <= batchsz-1
        :param index:
        :return:
        """
        # [setsz, 3, resize, resize]
        support_x = torch.FloatTensor(self.setsz, 3, self.resize, self.resize)
        # [setsz]
        support_y = np.zeros((self.setsz), dtype=np.int)
        # [querysz, 3, resize, resize]
        query_x = torch.FloatTensor(self.querysz, 3, self.resize, self.resize)
        # [querysz]
        query_y = np.zeros((self.querysz), dtype=np.int)

        self.support_x_batch=np.array(self.support_x_batch)
        self.query_x_batch=np.array(self.query_x_batch)

        flatten_support_x = self.support_x_batch[index].flatten()
        support_y = np.array(
            flatten_support_x//600).astype(np.int32)

        flatten_query_x = self.query_x_batch[index].flatten()
        #print(flatten_query_x)
        query_y = np.array(
            flatten_query_x//600).astype(np.int32)


        # print('global:', support_y, query_y)
        # support_y: [setsz]
        # query_y: [querysz]
        # unique: [n-way], sorted
        unique = np.unique(support_y)
        random.shuffle(unique)
        # relative means the label ranges from 0 to n-way
        support_y_relative = np.zeros(self.setsz)
        query_y_relative = np.zeros(self.querysz)
        for idx, l in enumerate(unique):
            support_y_relative[support_y == l] = idx
            query_y_relative[query_y == l] = idx

        # print('relative:', support_y_relative, query_y_relative)

        for i, id in enumerate(flatten_support_x):
            support_x[i] = self.transform(self.image_data[id])

        for i, id in enumerate(flatten_query_x):
            query_x[i] = self.transform(self.image_data[id])
        # print(support_set_y)
        # return support_x, torch.LongTensor(support_y), query_x, torch.LongTensor(query_y)

        return support_x, torch.LongTensor(support_y_relative), query_x, torch.LongTensor(query_y_relative)

    def __len__(self):
        # as we have built up to batchsz of sets, you can sample some small batch size of sets.
        return self.batchsz


class Net(torch.nn.Module):
    def __init__(self):
        super(Net,self).__init__()
        self.c1=torch.nn.Conv2d(in_channels = 3,out_channels = 32,kernel_size = 3,stride = 1,padding = 1)
        self.bn1=torch.nn.BatchNorm2d(32, affine=True)
        self.pool1=torch.nn.MaxPool2d(kernel_size = 2,stride = 2)#42*42
        self.c2=torch.nn.Conv2d(in_channels = 32,out_channels = 32,kernel_size = 3,stride = 1,padding = 1)
        self.bn2=torch.nn.BatchNorm2d(32, affine=True)
        self.pool2=torch.nn.MaxPool2d(kernel_size = 2,stride = 2)#21*21
        self.c3=torch.nn.Conv2d(in_channels = 32,out_channels = 32,kernel_size = 3,stride = 1,padding = 1)
        self.bn3=torch.nn.BatchNorm2d(32, affine=True)
        self.pool3=torch.nn.MaxPool2d(kernel_size = 2,stride = 2)#10*10
        self.c4=torch.nn.Conv2d(in_channels = 32,out_channels = 32,kernel_size = 3,stride = 1,padding = 1)
        self.bn4=torch.nn.BatchNorm2d(32, affine=True)
        self.pool4=torch.nn.MaxPool2d(kernel_size = 2,stride = 2)#5*5
        self.flat=torch.nn.Flatten()
        self.l1=torch.nn.Linear(in_features=32*5*5,out_features=n_way)
    
    def forward(self,x,param=None,affine=True):
        x=self.pool1(F.relu(self.bn1(self.c1(x))))
        x=self.pool2(F.relu(self.bn2(self.c2(x))))
        x=self.pool3(F.relu(self.bn3(self.c3(x))))
        x=self.pool4(F.relu(self.bn4(self.c4(x))))
        x=self.l1(self.flat(x))
        return x


class Meta(torch.nn.Module):
    def __init__(self):
        super(Meta, self).__init__()   
        self.update_lr = 0.04 
        self.test_lr=0.01
        self.meta_lr = 0.01 
        self.n_way = 5 
        self.k_shot = 1 
        self.k_query = 15 #
        self.update_step = 5 ## task-level inner update steps
        self.update_step_test = 10 
        
        self.net = Net() ## base-learner
        self.lossfunc=torch.nn.CrossEntropyLoss()  
        self.meta_optim = torch.optim.Adam(self.net.parameters(), lr = 0.001)
        

    def forward(self,x_support,y_support,x_query, y_query):
        task_num, task_size,chanel, h, w = x_support.size()#x_qry shape=torch.Size([4, 75, 3, 84, 84])
        accs=[]
        #lossh=0
       # meta_dict=self.net.state_dict()
        grads=[[] for s in range(task_num)]
        for i in range(task_num):
            net=deepcopy(self.net)
            for inner_step in range(self.update_step):
                logits = net(x_support[i])#[5,5]
                loss = F.cross_entropy(logits, y_support[i])
                #loss_ep.append(loss.item())
                net.zero_grad() 
                loss.backward()
                for f in net.parameters():
                    f.data.sub_(f.grad.data * self.update_lr)#inner_update
                if inner_step==self.update_step-1:
                    logits = net(x_query[i])
                    loss_task = F.cross_entropy(logits, y_query[i])
                    net.zero_grad()
                    loss_task.backward()
                    for f in net.parameters():
                        grads[i].append(f.grad.data)
                    with torch.no_grad():
                        pred_label=logits.argmax(dim=1)
                        acc=torch.eq(pred_label,y_query[i]).sum().item()/75
                        accs.append(acc)
            del net
        #self.net.load_state_dict(meta_dict)
        grads=np.array(grads)
        grads=np.mean(grads,axis=0)
        for f,g in zip(self.net.parameters(),grads):
            f.data.grad=g 
        self.meta_optim.step()
        return np.mean(accs)

           
    
    def finetunning(self, x_support, y_support, x_query, y_query):#torch.Size([5, 3, 84, 84]) torch.Size([75, 3, 84, 84])
        assert len(x_support.shape) == 4
        
        querysz = x_query.size(0)
        corrects = [0 for _ in range(self.update_step_test)]
        net = deepcopy(self.net)
        #print(next(net.parameters()).device)
        for test_step in range(self.update_step_test):
            logits = net(x_support)
            loss = F.cross_entropy(logits, y_support)
            net.zero_grad()
            loss.backward()
            for f in net.parameters():
                f.data.sub_(f.grad.data * self.test_lr)
            with torch.no_grad():
                pred_logit=net(x_query)#[75,5]
                pred_label=pred_logit.argmax(dim=1)
                acc=torch.eq(pred_label,y_query).sum().item()/querysz
                corrects[test_step]=acc
        return corrects

epochs=1000001  
device = "cuda:0" if torch.cuda.is_available() else "cpu"
if __name__ == '__main__':
    traindata=MiniImagenet(mode='train', n_way=5, k_shot=1,
                        k_query=15,
                        batchsz=10000, resize=84)
    mini_test = MiniImagenet(mode='test', n_way=5, k_shot=1,
                             k_query=15,
                             batchsz=100, resize=84)
    meta=Meta().to(device)
    test_acc_trac=[]
    for epoch in range(epochs//2500):
        dl=DataLoader(traindata,4,shuffle=True,num_workers=1, pin_memory=True)
        for step, (x_spt, y_spt, x_qry, y_qry) in enumerate(dl):#x_qry shape=torch.Size([4, 75, 3, 84, 84])
            x_spt, y_spt, x_qry, y_qry = x_spt.to(device), y_spt.to(device), x_qry.to(device), y_qry.to(device)
            acc=meta(x_spt, y_spt,x_qry,y_qry)
            if (epoch*2500+step)%100==0:
                print("total step",epoch*2500+step,"\ttrain acc:",acc)
            if (epoch*2500+step) % 1000 == 0:  # evaluation
                db_test = DataLoader(mini_test, 1, shuffle=True, num_workers=1, pin_memory=True)
                accs_all_test = []
                for x_spt, y_spt, x_qry, y_qry in db_test:#100epoch
                    #torch.Size([1, 5, 3, 84, 84]) torch.Size([1, 75, 3, 84, 84])
                    x_spt, y_spt, x_qry, y_qry = x_spt.squeeze(0).to(device), y_spt.squeeze(0).to(device), \
                                                    x_qry.squeeze(0).to(device), y_qry.squeeze(0).to(device)
                    #torch.Size([5, 3, 84, 84]) torch.Size([75, 3, 84, 84])

                    accs = meta.finetunning(x_spt, y_spt, x_qry, y_qry)
                    accs_all_test.append(accs)

                # [b, update_step+1]
                accs = np.array(accs_all_test).mean(axis=0).astype(np.float16)
                print('Test acc:', accs)
                test_acc_trac.append(accs)
                np.save('macc_adam1.npy',test_acc_trac)

















