import numpy as np
import pandas as pd
import random
import torch
from torch import nn
from torchvision.transforms import transforms
from torch.utils.data import Dataset
from torch.utils.data import DataLoader

from collections import Counter

import torch.nn.functional as F
from copy import deepcopy
espace_dim=1600
class MiniImagenet(Dataset):
    def __init__(self, mode, batchsz, n_way, k_shot, k_query, resize, startidx=0):
        self.batchsz = batchsz  # batch of set, not batch of imgs
        self.n_way = n_way  # n-way
        self.k_shot = k_shot  # k-shot
        self.k_query = k_query  # for evaluation
        self.setsz = self.n_way * self.k_shot  # num of samples per set
        self.querysz = self.n_way * self.k_query  # number of samples per set for evaluation
        self.resize = resize 
       

        if mode == 'train':
            self.transform = transforms.Compose([
                                                 transforms.ToTensor(),
                                                 transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225))
                                                 ])
        else:
            self.transform = transforms.Compose([
                                                 transforms.ToTensor(),
                                                 transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225))
                                                 ])

        self.data=pd.read_pickle('mini-imagenet-cache-'+ mode +'.pkl')#dict{'image_data":[n*84*84*3],'class_dict':{'n12345678':[0,...,599],..}}
        self.image_data=self.data['image_data']
        self.class_dict=self.data['class_dict']
        self.cls_num=len(self.class_dict)
        self.create_batch(self.batchsz)
        #self.data = []
    
    def create_batch(self, batchsz):
        """
        create batch for meta-learning.
        ×episode× here means batch, and it means how many sets we want to retain.
        :param episodes: batch size
        :return:
        """

        self.support_x_batch = []  # support set batch
        self.query_x_batch = []  # query set batch
        for b in range(batchsz):  # for each batch
            # 1.select n_way classes randomly
            selected_cls = np.random.choice(self.cls_num, self.n_way, False)  # no duplicate
            np.random.shuffle(selected_cls)
            support_x = []
            query_x = []
            for cls in selected_cls:
                # 2. select k_shot + k_query for each class
                selected_imgs_idx = np.random.choice(600, self.k_shot + self.k_query, False)
                np.random.shuffle(selected_imgs_idx)
                indexDtrain = np.array(selected_imgs_idx[:self.k_shot])  # idx for Dtrain
                indexDtest = np.array(selected_imgs_idx[self.k_shot:])  # idx for Dtest
                support_x.append(
                    cls*600+indexDtrain.tolist())  # get all images index for current Dtrain
                query_x.append(cls*600+indexDtest.tolist())

            # shuffle the correponding relation between support set and query set
            random.shuffle(support_x)
            random.shuffle(query_x)

            self.support_x_batch.append(support_x)  # append set to current sets
            self.query_x_batch.append(query_x)  # append sets to current sets
            #support_x_batch  shape=(batchsz,way,shot)
        
    def __getitem__(self, index):
        """
        index means index of sets, 0<= index <= batchsz-1
        :param index:
        :return:
        """
        # [setsz, 3, resize, resize]
        support_x = torch.FloatTensor(self.setsz, 3, self.resize, self.resize)
        # [setsz]
        support_y = np.zeros((self.setsz), dtype=np.int)
        # [querysz, 3, resize, resize]
        query_x = torch.FloatTensor(self.querysz, 3, self.resize, self.resize)
        # [querysz]
        query_y = np.zeros((self.querysz), dtype=np.int)

        self.support_x_batch=np.array(self.support_x_batch)
        self.query_x_batch=np.array(self.query_x_batch)

        flatten_support_x = self.support_x_batch[index].flatten()
        support_y = np.array(
            flatten_support_x//600).astype(np.int32)

        flatten_query_x = self.query_x_batch[index].flatten()
        #print(flatten_query_x)
        query_y = np.array(
            flatten_query_x//600).astype(np.int32)


        # print('global:', support_y, query_y)
        # support_y: [setsz]
        # query_y: [querysz]
        # unique: [n-way], sorted
        unique = np.unique(support_y)
        random.shuffle(unique)
        # relative means the label ranges from 0 to n-way
        support_y_relative = np.zeros(self.setsz)
        query_y_relative = np.zeros(self.querysz)
        for idx, l in enumerate(unique):
            support_y_relative[support_y == l] = idx
            query_y_relative[query_y == l] = idx

        # print('relative:', support_y_relative, query_y_relative)

        for i, id in enumerate(flatten_support_x):
            support_x[i] = self.transform(self.image_data[id])

        for i, id in enumerate(flatten_query_x):
            query_x[i] = self.transform(self.image_data[id])
        # print(support_set_y)
        # return support_x, torch.LongTensor(support_y), query_x, torch.LongTensor(query_y)

        return support_x, torch.LongTensor(support_y_relative), query_x, torch.LongTensor(query_y_relative)

    def __len__(self):
        # as we have built up to batchsz of sets, you can sample some small batch size of sets.
        return self.batchsz


class ProtoNet(torch.nn.Module):

    def __init__(self):
        super(ProtoNet,self).__init__()
        self.n_way_train=20#30 for 1shot 20 for 5 shot
        self.k_shot=5
        self.k_query=20
        self.lr=0.01
        self.espace_dim=1000
        self.c1=torch.nn.Conv2d(in_channels = 3,out_channels = 32,kernel_size = 3,stride = 1,padding = 1)
        self.bn1=torch.nn.BatchNorm2d(32, affine=True)
        self.pool1=torch.nn.MaxPool2d(kernel_size = 2,stride = 2)#42*42
        self.c2=torch.nn.Conv2d(in_channels = 32,out_channels = 32,kernel_size = 3,stride = 1,padding = 1)
        self.bn2=torch.nn.BatchNorm2d(32, affine=True)
        self.pool2=torch.nn.MaxPool2d(kernel_size = 2,stride = 2)#21*21
        self.c3=torch.nn.Conv2d(in_channels = 32,out_channels = 32,kernel_size = 3,stride = 1,padding = 1)
        self.bn3=torch.nn.BatchNorm2d(32, affine=True)
        self.pool3=torch.nn.MaxPool2d(kernel_size = 2,stride = 2)#10*10
        self.c4=torch.nn.Conv2d(in_channels = 32,out_channels = 32,kernel_size = 3,stride = 1,padding = 1)
        self.bn4=torch.nn.BatchNorm2d(32, affine=True)
        self.pool4=torch.nn.MaxPool2d(kernel_size = 2,stride = 2)#5*5
        self.flat=torch.nn.Flatten()
        self.l1=torch.nn.Linear(in_features=32*5*5,out_features=self.espace_dim)
    
    def forward(self,x,param=None,affine=True):
        task_num=x.size()[0]
        for i in range(task_num):
            proto=self.pool1(F.relu(self.bn1(self.c1(x[i]))))
            proto=self.pool2(F.relu(self.bn2(self.c2(proto))))
            proto=self.pool3(F.relu(self.bn3(self.c3(proto))))
            proto=self.pool4(F.relu(self.bn4(self.c4(proto))))
            proto=self.l1(self.flat(proto))
            print(proto.size())#torch.Size([100, 1000])
            proto=torch.mean(proto,dim=0)
            print(proto.size())
            s
        return proto
       

epochs=100001  
device = "cuda:0" if torch.cuda.is_available() else "cpu"
if __name__ == '__main__':
    torch.manual_seed(5)
    torch.cuda.manual_seed_all(5)
    np.random.seed(5)

 
    device = torch.device('cuda:0')
    pnet = ProtoNet().to(device)
    mini_train = MiniImagenet(mode='train', n_way=pnet.n_way_train, k_shot=pnet.k_shot,
                        k_query=pnet.k_query,
                        batchsz=10000, resize=84)
    mini_test = MiniImagenet(mode='test', n_way=5, k_shot=1,
                             k_query=15,
                             batchsz=100, resize=84)

    for epoch in range(epochs//1250):
        # fetch meta_batchsz num of episode each time
        db = DataLoader(mini_train, 8, shuffle=True, num_workers=1, pin_memory=True)

        for step, (x_spt, y_spt, x_qry, y_qry) in enumerate(db):

            x_spt, y_spt, x_qry, y_qry = x_spt.to(device), y_spt.to(device), x_qry.to(device), y_qry.to(device)
            pnet(x_spt)
            
            
           

    














