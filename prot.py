import numpy as np
import pandas as pd
import random
import torch
from torch import nn
from torchvision.transforms import transforms
from torch.utils.data import Dataset
from torch.utils.data import DataLoader

from collections import Counter

import torch.nn.functional as F
from copy import deepcopy
espace_dim=1600
class MiniImagenet(Dataset):
    def __init__(self, mode, batchsz, n_way, k_shot, k_query, resize, startidx=0):
        self.batchsz = batchsz  # batch of set, not batch of imgs
        self.n_way = n_way  # n-way
        self.k_shot = k_shot  # k-shot
        self.k_query = k_query  # for evaluation
        self.setsz = self.n_way * self.k_shot  # num of samples per set
        self.querysz = self.n_way * self.k_query  # number of samples per set for evaluation
        self.resize = resize 
       

        if mode == 'train':
            self.transform = transforms.Compose([
                                                 transforms.ToTensor(),
                                                 transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225))
                                                 ])
        else:
            self.transform = transforms.Compose([
                                                 transforms.ToTensor(),
                                                 transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225))
                                                 ])

        self.data=pd.read_pickle('mini-imagenet-cache-'+ mode +'.pkl')#dict{'image_data":[n*84*84*3],'class_dict':{'n12345678':[0,...,599],..}}
        self.image_data=self.data['image_data']
        self.class_dict=self.data['class_dict']
        self.cls_num=len(self.class_dict)
        self.create_batch(self.batchsz)
        #self.data = []
    
    def create_batch(self, batchsz):
        """
        create batch for meta-learning.
        ×episode× here means batch, and it means how many sets we want to retain.
        :param episodes: batch size
        :return:
        """

        self.support_x_batch = []  # support set batch
        self.query_x_batch = []  # query set batch
        for b in range(batchsz):  # for each batch
            # 1.select n_way classes randomly
            selected_cls = np.random.choice(self.cls_num, self.n_way, False)  # no duplicate
            np.random.shuffle(selected_cls)
            support_x = []
            query_x = []
            for cls in selected_cls:
                # 2. select k_shot + k_query for each class
                selected_imgs_idx = np.random.choice(600, self.k_shot + self.k_query, False)
                np.random.shuffle(selected_imgs_idx)
                indexDtrain = np.array(selected_imgs_idx[:self.k_shot])  # idx for Dtrain
                indexDtest = np.array(selected_imgs_idx[self.k_shot:])  # idx for Dtest
                support_x.append(
                    cls*600+indexDtrain.tolist())  # get all images index for current Dtrain
                query_x.append(cls*600+indexDtest.tolist())

            # shuffle the correponding relation between support set and query set
            random.shuffle(support_x)
            random.shuffle(query_x)

            self.support_x_batch.append(support_x)  # append set to current sets
            self.query_x_batch.append(query_x)  # append sets to current sets
            #support_x_batch  shape=(batchsz,way,shot)
        
    def __getitem__(self, index):
        """
        index means index of sets, 0<= index <= batchsz-1
        :param index:
        :return:
        """
        # [setsz, 3, resize, resize]
        support_x = torch.FloatTensor(self.setsz, 3, self.resize, self.resize)
        # [setsz]
        support_y = np.zeros((self.setsz), dtype=np.int)
        # [querysz, 3, resize, resize]
        query_x = torch.FloatTensor(self.querysz, 3, self.resize, self.resize)
        # [querysz]
        query_y = np.zeros((self.querysz), dtype=np.int)

        self.support_x_batch=np.array(self.support_x_batch)
        self.query_x_batch=np.array(self.query_x_batch)

        flatten_support_x = self.support_x_batch[index].flatten()
        support_y = np.array(
            flatten_support_x//600).astype(np.int32)

        flatten_query_x = self.query_x_batch[index].flatten()
        #print(flatten_query_x)
        query_y = np.array(
            flatten_query_x//600).astype(np.int32)


        # print('global:', support_y, query_y)
        # support_y: [setsz]
        # query_y: [querysz]
        # unique: [n-way], sorted
        unique = np.unique(support_y)
        random.shuffle(unique)
        # relative means the label ranges from 0 to n-way
        support_y_relative = np.zeros(self.setsz)
        query_y_relative = np.zeros(self.querysz)
        for idx, l in enumerate(unique):
            support_y_relative[support_y == l] = idx
            query_y_relative[query_y == l] = idx

        # print('relative:', support_y_relative, query_y_relative)

        for i, id in enumerate(flatten_support_x):
            support_x[i] = self.transform(self.image_data[id])

        for i, id in enumerate(flatten_query_x):
            query_x[i] = self.transform(self.image_data[id])
        # print(support_set_y)
        # return support_x, torch.LongTensor(support_y), query_x, torch.LongTensor(query_y)

        return support_x, torch.LongTensor(support_y_relative), query_x, torch.LongTensor(query_y_relative)

    def __len__(self):
        # as we have built up to batchsz of sets, you can sample some small batch size of sets.
        return self.batchsz


class Learner(nn.Module):
    """
    定义一个网络
    """
    def __init__(self, config):
        super(Learner, self).__init__()
        self.config = config ## 对模型各个超参数的定义
        self.vars = nn.ParameterList() ## 这个字典中包含了所有需要被优化的tensor
        self.vars_bn = nn.ParameterList()  
        
        for i, (name, param) in enumerate(self.config):
            if name == 'conv2d':
                ## [ch_out, ch_in, kernel_size, kernel_size]
                weight = nn.Parameter(torch.ones(*param[:4])) ## 产生*param大小的全为1的tensor
                torch.nn.init.kaiming_normal_(weight) ## 初始化权重
                self.vars.append(weight) ## 加到nn.ParameterList中
                
                bias = nn.Parameter(torch.zeros(param[0]))
                self.vars.append(bias)
                
            elif name == 'linear':
                weight = nn.Parameter(torch.ones(*param))
                torch.nn.init.kaiming_normal_(weight)
                self.vars.append(weight)
                bias  = nn.Parameter(torch.zeros(param[0]))
                self.vars.append(bias)
            
            elif name == 'bn':
                ## 对小批量(mini-batch)的2d或3d输入进行批标准化(Batch Normalization)操作,
                ## BN层在训练过程中，会将一个Batch的中的数据转变成正态分布
                weight = nn.Parameter(torch.ones(param[0]))
                self.vars.append(weight)
                bias = nn.Parameter(torch.zeros(param[0]))
                self.vars.append(bias)
                
                ### 
                running_mean = nn.Parameter(torch.zeros(param[0]), requires_grad = False)
                running_var = nn.Parameter(torch.zeros(param[0]), requires_grad = False)
                
                self.vars_bn.extend([running_mean, running_var]) ## 在列表附加参数
                
            elif name in ['tanh', 'relu', 'upsample', 'avg_pool2d', 'max_pool2d',
                          'flatten', 'reshape', 'leakyrelu', 'sigmoid']:
                continue
                
            else:
                raise NotImplementedError       
    
    
    def forward(self, x, vars = None, bn_training=True):
        '''
        :param bn_training: set False to not update
        :return: 
        '''
        
        if vars is None:
            vars = self.vars
            
        idx = 0 ; bn_idx = 0
        for name, param in self.config:
            if name == 'conv2d':
                weight, bias = vars[idx], vars[idx + 1]
                x = F.conv2d(x, weight, bias, stride = param[4], padding = param[5]) 
                idx += 2
                
            elif name == 'linear':
                weight, bias = vars[idx], vars[idx + 1]
                x = F.linear(x, weight, bias)
                idx += 2
                
            elif name == 'bn':
                weight, bias = vars[idx], vars[idx + 1]
                running_mean, running_var = self.vars_bn[bn_idx], self.vars_bn[bn_idx + 1]
                x = F.batch_norm(x, running_mean, running_var, weight= weight, bias = bias, training = bn_training)
                idx += 2
                bn_idx += 2
            
            elif name == 'flatten':
                x = x.view(x.size(0), -1)
            
            elif name == 'relu':
                x = F.relu(x, inplace = [param[0]])
            
            elif name == 'reshape':
                # [b, 8] => [b, 2, 2, 2]
                x = x.view(x.size(0), *param)
            elif name == 'leakyrelu':
                x = F.leaky_relu(x, negative_slope=param[0], inplace=param[1])
            elif name == 'tanh':
                x = F.tanh(x)
            elif name == 'sigmoid':
                x = torch.sigmoid(x)
            elif name == 'upsample':
                x = F.upsample_nearest(x, scale_factor=param[0])
            elif name == 'max_pool2d':
                x = F.max_pool2d(x, param[0], param[1], param[2])
            elif name == 'avg_pool2d':
                x = F.avg_pool2d(x, param[0], param[1], param[2])
            else:
                raise NotImplementedError
            
        assert idx == len(vars)
        assert bn_idx == len(self.vars_bn)
        
        return x
    
    
    
    def parameters(self):
        
        return self.vars

class Proto(nn.Module):
    def __init__(self, config):
        super(Proto, self).__init__()   
        self.lr = 0.01 ## learner中的学习率，即\alpha
        self.n_way_train = 20 ## 30 for 1shot / 20 for 5shot
        self.k_shot = 5 
        self.k_query = 15 ## 15个查询样本
        self.update_step = 5 
        
        self.net = Learner(config) ## base-learner
        self.optim = torch.optim.Adam(self.net.parameters(), lr = self.lr)
        
    def forward(self, x_support, y_support, x_query, y_query):#torch.Size([8, 100, 3, 84, 84]) torch.Size([8, 300, 3, 84, 84])
        
        f=nn.CrossEntropyLoss()
        loss=0
        losst=0
        task_num, ways, shots, h, w = x_support.size()
        for i in range(task_num):
            characters=[[] for way in range (self.n_way_train)]
            chs=self.net(x_support[i],vars=None,bn_training=True)#torch.Size([100, 1600])
            for shot in range(self.n_way_train*self.k_shot):
                characters[y_support[i][shot]].append(chs[shot])
            prototype=torch.mean(characters,dim=1)
            losst+=torch.mean(prototype)
            chs_qry=self.net(x_query[i],vars=None,bn_training=True)#[300,1600]
            distance=[[] for query in range(self.n_way_train*self.k_query)]
            for query in range (self.n_way_train*self.k_query):
                for way in range(self.n_way_train):
                    d=((chs_qry[query]-prototype[way])*(chs_qry[query]-prototype[way])).sum()
                    distance[query].append(d)
            distance=torch.Tensor(distance)#[300,20]
            prob=F.softmax(distance,dim=1).to(device)
            loss+=(f(prob,y_query[i])/self.n_way_train*self.k_query)
        losst=losst/task_num
        self.optim.zero_grad()
        losst.backward()
        self.optim.step()
        print(losst)


            


#       


        
        

epochs=100001  
device = "cuda:0" if torch.cuda.is_available() else "cpu"
if __name__ == '__main__':
    torch.manual_seed(5)
    torch.cuda.manual_seed_all(5)
    np.random.seed(5)
    acc_trac=[]

    config = [
        ('conv2d', [32, 3, 3, 3, 1, 1]),
        ('relu', [True]),
        ('bn', [32]),
        ('max_pool2d', [2, 2, 0]),
        ('conv2d', [32, 32, 3, 3, 1, 1]),
        ('relu', [True]),
        ('bn', [32]),
        ('max_pool2d', [2, 2, 0]),
        ('conv2d', [32, 32, 3, 3, 1, 1]),
        ('relu', [True]),
        ('bn', [32]),
        ('max_pool2d', [2, 2, 0]),
        ('conv2d', [32, 32, 3, 3, 1, 1]),
        ('relu', [True]),
        ('bn', [32]),
        ('max_pool2d', [2, 2, 0]),
        ('flatten', []),
        ('linear', [espace_dim, 32 * 5 * 5]),
    ]

    device = torch.device('cuda:0')
    pnet = Proto(config).to(device)
    mini_train = MiniImagenet(mode='train', n_way=pnet.n_way_train, k_shot=pnet.k_shot,
                        k_query=pnet.k_query,
                        batchsz=10000, resize=84)
    mini_test = MiniImagenet(mode='test', n_way=5, k_shot=1,
                             k_query=15,
                             batchsz=100, resize=84)

    for epoch in range(epochs//1250):
        # fetch meta_batchsz num of episode each time
        db = DataLoader(mini_train, 8, shuffle=True, num_workers=1, pin_memory=True)

        for step, (x_spt, y_spt, x_qry, y_qry) in enumerate(db):

            x_spt, y_spt, x_qry, y_qry = x_spt.to(device), y_spt.to(device), x_qry.to(device), y_qry.to(device)
            pnet(x_spt, y_spt, x_qry, y_qry)
            
            
           

    














